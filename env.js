{
	"id": "1a7d20f0-8ad1-4c5f-8a96-7b4ff492a77f",
	"name": "Finan-dev",
	"values": [
		{
			"key": "finan_domain_api",
			"value": "https://dev-api-v2.finan.vn",
			"enabled": true
		},
		{
			"key": "code_id",
			"value": "",
			"enabled": true
		},
		{
			"key": "otp",
			"value": "",
			"enabled": true
		},
		{
			"key": "wrong_otp",
			"value": "",
			"enabled": true
		},
		{
			"key": "phoneNumber",
			"value": "",
			"enabled": true
		},
		{
			"key": "device_id",
			"value": "",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2021-09-12T10:44:23.236Z",
	"_postman_exported_using": "Postman/8.12.2"
}