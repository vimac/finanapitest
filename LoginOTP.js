{
	"info": {
		"_postman_id": "a40be6e7-c65f-4fbd-a71f-12de93ae665c",
		"name": "FINANApiTest",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "Micro Services",
			"item": [
				{
					"name": "MS User Management",
					"item": [
						{
							"name": "Login OTP",
							"item": [
								{
									"name": "Generate OTP",
									"event": [
										{
											"listen": "test",
											"script": {
												"exec": [
													"var jsonDataReponse = pm.response.json();",
													"var code_id = jsonDataReponse.code;",
													"    pm.environment.set('code_id', code_id);",
													"pm.test(\"Status code is 200 and return code\", function () {",
													"    pm.response.to.have.status(200);",
													"    pm.expect(jsonDataReponse.code).to.be.a(\"number\");",
													"    pm.expect(jsonDataReponse.code).to.eql(pm.environment.get('code_id'));",
													"    pm.expect(jsonDataReponse.data.max_failed_times).to.eql(5);",
													"    pm.expect(jsonDataReponse.data.status).to.eql(\"created\");",
													"});"
												],
												"type": "text/javascript"
											}
										},
										{
											"listen": "prerequest",
											"script": {
												"exec": [
													"function getRandomNumberBetween(min,max){",
													"  return Math.floor(Math.random()*(max-min+1)+min);",
													"}",
													"var phoneNumber = \"09\"+ getRandomNumberBetween(0,9) + \"123456\" + getRandomNumberBetween(0,9);",
													"pm.environment.set(\"phoneNumber\", phoneNumber);",
													"",
													"function deviceId() {",
													"  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {",
													"    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);",
													"    return v.toString(16);",
													"  });",
													"}",
													"var device_id = deviceId();",
													"pm.environment.set(\"device_id\",device_id);"
												],
												"type": "text/javascript"
											}
										}
									],
									"request": {
										"method": "POST",
										"header": [
											{
												"key": "Content-Type",
												"value": "application/json"
											}
										],
										"body": {
											"mode": "raw",
											"raw": "{\n\t\"phone_number\" : \"{{phoneNumber}}\",\n    \"platform\" : \"web_buyer\",\n    \"device_id\" : \"{{device_id}}\"\n}"
										},
										"url": {
											"raw": "{{finan_domain_api}}/generate-otp",
											"host": [
												"{{finan_domain_api}}"
											],
											"path": [
												"generate-otp"
											]
										}
									},
									"response": []
								},
								{
									"name": "Confirm OTP_CorrectOTP",
									"event": [
										{
											"listen": "test",
											"script": {
												"exec": [
													"var jsonDataResponse = pm.response.json();",
													"pm.test(\"Status code is 200 and return message\", function () {",
													"    pm.response.to.have.status(200);",
													"    pm.expect(jsonDataResponse.message).to.eql('Success');",
													"});"
												],
												"type": "text/javascript"
											}
										},
										{
											"listen": "prerequest",
											"script": {
												"exec": [
													"function getRandomNumberBetween(min,max){",
													"  return Math.floor(Math.random()*(max-min+1)+min);",
													"}",
													"var phoneNumber = \"09\"+ getRandomNumberBetween(0,9) + \"123456\" + getRandomNumberBetween(0,9);",
													"pm.environment.set(\"phoneNumber\", phoneNumber);",
													"",
													"function deviceId() {",
													"  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {",
													"    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);",
													"    return v.toString(16);",
													"  });",
													"}",
													"var device_id = deviceId();",
													"pm.environment.set(\"device_id\",device_id);"
												],
												"type": "text/javascript"
											}
										}
									],
									"request": {
										"method": "POST",
										"header": [
											{
												"key": "Content-Type",
												"value": "application/json"
											}
										],
										"body": {
											"mode": "raw",
											"raw": "{\n    \"phone_number\" : \"{{phoneNumber}}\",\n    \"otp\" : \"{{otp}}\",\n    \"device_id\": \"{{device_id}}\",\n    \"platform\" : \"web_buyer\",\n    \"remember_me\" : true\n}"
										},
										"url": {
											"raw": "{{finan_domain_api}}/confirm-otp",
											"host": [
												"{{finan_domain_api}}"
											],
											"path": [
												"confirm-otp"
											]
										}
									},
									"response": []
								},
								{
									"name": "Confirm OTP_WrongOTP",
									"event": [
										{
											"listen": "test",
											"script": {
												"exec": [
													"var jsonDataResponse = pm.response.json();",
													"pm.test(\"Status code is 200 and return message is wrong otp\", function () {",
													"    pm.response.to.have.status(200);",
													"});",
													"",
													"pm.test(\"Return message is wrong otp\", function () {",
													"    pm.expect(jsonDataResponse.message).to.eql('Success');",
													"    pm.expect(jsonDataResponse.data.status).to.eql('wrong_otp');",
													"    pm.expect(jsonDataResponse.data.message).to.eql('Wrong OTP, Please Recheck');",
													"});"
												],
												"type": "text/javascript"
											}
										},
										{
											"listen": "prerequest",
											"script": {
												"exec": [
													"pm.environment.set(\"wrong_otp\", \"00000\");",
													"",
													"function getRandomNumberBetween(min,max){",
													"  return Math.floor(Math.random()*(max-min+1)+min);",
													"}",
													"var phoneNumber = \"09\"+ getRandomNumberBetween(0,9) + \"123456\" + getRandomNumberBetween(0,9);",
													"pm.environment.set(\"phoneNumber\", phoneNumber);",
													"",
													"function deviceId() {",
													"  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {",
													"    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);",
													"    return v.toString(16);",
													"  });",
													"}",
													"var device_id = deviceId();",
													"pm.environment.set(\"device_id\",device_id);"
												],
												"type": "text/javascript"
											}
										}
									],
									"request": {
										"method": "POST",
										"header": [
											{
												"key": "Content-Type",
												"value": "application/json"
											}
										],
										"body": {
											"mode": "raw",
											"raw": "{\n    \"phone_number\" : \"{{phoneNumber}}\",\n    \"otp\" : \"{{wrong_otp}}\",\n    \"device_id\": \"{{device_id}}\",\n    \"platform\" : \"web_buyer\",\n    \"remember_me\" : true\n}"
										},
										"url": {
											"raw": "{{finan_domain_api}}/confirm-otp",
											"host": [
												"{{finan_domain_api}}"
											],
											"path": [
												"confirm-otp"
											]
										}
									},
									"response": []
								},
								{
									"name": "Confirm OTP_ExpiredOTP",
									"event": [
										{
											"listen": "test",
											"script": {
												"exec": [
													"var jsonDataResponse = pm.response.json();",
													"pm.test(\"Status code is 200 and return message is wrong otp\", function () {",
													"    pm.response.to.have.status(200);",
													"});",
													"",
													"pm.test(\"Return message is wrong otp\", function () {",
													"    pm.expect(jsonDataResponse.message).to.eql('Success');",
													"    pm.expect(jsonDataResponse.data.status).to.eql('wrong_otp');",
													"    pm.expect(jsonDataResponse.data.message).to.eql('Wrong OTP, Please Recheck');",
													"});"
												],
												"type": "text/javascript"
											}
										},
										{
											"listen": "prerequest",
											"script": {
												"exec": [
													"pm.environment.set(\"wrong_otp\", \"00000\");",
													"",
													"function getRandomNumberBetween(min,max){",
													"  return Math.floor(Math.random()*(max-min+1)+min);",
													"}",
													"var phoneNumber = \"09\"+ getRandomNumberBetween(0,9) + \"123456\" + getRandomNumberBetween(0,9);",
													"pm.environment.set(\"phoneNumber\", phoneNumber);",
													"",
													"function deviceId() {",
													"  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {",
													"    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);",
													"    return v.toString(16);",
													"  });",
													"}",
													"var device_id = deviceId();",
													"pm.environment.set(\"device_id\",device_id);"
												],
												"type": "text/javascript"
											}
										}
									],
									"request": {
										"method": "POST",
										"header": [
											{
												"key": "Content-Type",
												"value": "application/json"
											}
										],
										"body": {
											"mode": "raw",
											"raw": "{\n    \"phone_number\" : \"{{phoneNumber}}\",\n    \"otp\" : \"{{wrong_otp}}\",\n    \"device_id\": \"{{device_id}}\",\n    \"platform\" : \"web_buyer\",\n    \"remember_me\" : true\n}"
										},
										"url": {
											"raw": "{{finan_domain_api}}/confirm-otp",
											"host": [
												"{{finan_domain_api}}"
											],
											"path": [
												"confirm-otp"
											]
										}
									},
									"response": []
								}
							]
						}
					]
				}
			]
		}
	]
}